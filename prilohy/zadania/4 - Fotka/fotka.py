# Vytvorte funkciu, ktorá zistí či na fotke je tvár,
# polia znázorňujú pixely obrázkov
def is_face_on_photo(photo):
    length = len(photo)
    arr = ["f","a","c","e"]

    return result

# Testy:
print is_face_on_photo([['f', 'a'],['c', 'e']]) # True
print is_face_on_photo([['f', 'a', 'c', 'e']]) # False
print is_face_on_photo([['e','c','x'], ['a','f','x'],['x','x','x']]) # True
print is_face_on_photo([['f','f','x'], ['a','a','x'],['x','x','x']]) # False